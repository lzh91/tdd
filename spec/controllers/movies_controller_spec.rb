require 'spec_helper'

describe MoviesController do
  render_views

  describe "Find Movies with Same Director" do
    it 'should provide a RESTFUL route to find similar movies' do
      { :get => '/movies/12/similar_movies'}.
        should route_to("movies#similar_movies",
                        :id => '12')
    end
    it 'should call a model method to find movies whose director
matches that of the current movie' do
      fake_results = [double('movie1'), double('movie2')]
      Movie.should_receive('where').
        with(hash_including 'director' => 'Tom').
        and_return(fake_results)
      Movie.find_similar_movies('Tom')
    end
    describe "test on controller method similar_movies" do
      before :each do
        @movie1 = Movie.create!(:title => 'Star Wars',
                                :rating => 'PG',
                                :director => 'George Lucas',
                                :release_date => '1977-05-25')
        @movie2 = Movie.create!(:title => 'Blade Runner',
                                :rating => 'PG',
                                :director => 'Ridley Scott',
                                :release_date => '1982-06-25')
        @movie3 = Movie.create!(:title => 'Alien',
                                :rating => 'R',
                                :director => '',
                                :release_date => '1979-05-25')
        @movie4 = Movie.create!(:title => 'THX-1138',
                                :rating => 'R',
                                :director => 'George Lucas',
                                :release_date => '1971-03-11')
      end
      it 'should redirect to the home page when director is not valid' do
        fake_results = [double('movie1'), double('movie2')]
        post :similar_movies, {:id => '3'}
        response.should redirect_to('/')
      end
      describe "check the happy path of the find similar movies" do
        before :each do
          Movie.should_receive('find_similar_movies').
            with(Movie.find('1').director).
            and_return([@movie1, @movie4])
          post :similar_movies, {:id => '1'}
        end
        it 'should call a controller method to receive the click on
"Find With Same Director", and grab the id of the subject' do
          assigns(:id).should == '1'
        end
        it 'should render a view if the director is valid' do
          response.should render_template('similar_movies')
        end
      end
    end
  end
end
